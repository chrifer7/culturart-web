<!DOCTYPE html>
<html>
  <head>
    <title>E-QUIPU</title>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <style>
		html, body, #map-canvas {
			height: 100%;
			margin: 0px;
			padding: 0px
		}
	  
		#over_map { position: absolute; background-color: transparent; top: 10px; left: 10px; z-index: 99; background: white; }
		#over_map_right { position: absolute; background-color: transparent; top: 40px; right: 10px; z-index: 99; background: white; }
    </style>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
    <script>
		'use strinct';
		function initialize() {
			
			var myLating=new google.maps.LatLng(-12.075938,-77.014441);
			var mapOptions = {
			zoom: 12,
			center: new google.maps.LatLng(-12.075938,-77.014441)
			};
			
			var map = new google.maps.Map(document.getElementById('map-canvas'),
			  mapOptions);
	
			
			
			google.maps.event.addListener(map, 'click', function(e) {
				if(document.getElementById('namePOI').value==''){
					alert('Escriba una descripcion al lugar');
					 document.getElementById('namePOI').focus()
					return false;
				}
				placeMarker(e.latLng, map);
				infoWin(e.latLng, map,document.getElementById('namePOI').value);
				document.getElementById('namePOI').value='';
			});
			
			
		}
	
		function placeMarker(position, map) {
			marker = new google.maps.Marker({
			position: position,
			map: map
			});
			map.panTo(position);
		}
		
		function infoWin(position, map,content){
			var infowindow = new google.maps.InfoWindow({
				content: content,
				position:position
			});
			infowindow.open(map,marker);
		}
		
		google.maps.event.addDomListener(window, 'load', initialize);

    </script>
  </head>
  <body>
    <div id="map-canvas"></div>
	
	<div id="over_map_right">
				
			<p>
			<input type="text" required="required" placeholder="Lugar" value="" onclick="this.value=''" name="namePOI" id="namePOI">
			</p>
					
					
			</div>
	
  </body>
</html>

