'use strinct';
function initialize() {
	
	var myLating=new google.maps.LatLng(-12.075938,-77.014441);
	var mapOptions = {
	zoom: 12,
	center: new google.maps.LatLng(-12.075938,-77.014441)
	};
	
	var map = new google.maps.Map(document.getElementById('map-canvas'),
	  mapOptions);

	
	
	google.maps.event.addListener(map, 'click', function(e) {
		if(document.getElementById('namePOI').value==''){
			alert('Escriba una descripcion al lugar');
			 document.getElementById('namePOI').focus()
			return false;
		}
		placeMarker(e.latLng, map);
		infoWin(e.latLng, map,document.getElementById('namePOI').value);
		document.getElementById('namePOI').value='';
	});
	
	//jQuery section
	var map_w = jQuery('.padheader .container_24').width();
	jQuery('#map-canvas').width(map_w);
	
}

function placeMarker(position, map) {
	marker = new google.maps.Marker({
	position: position,
	map: map
	});
	map.panTo(position);
}

function infoWin(position, map,content){
	var infowindow = new google.maps.InfoWindow({
		content: content,
		position:position
	});
	infowindow.open(map,marker);
}

google.maps.event.addDomListener(window, 'load', initialize);