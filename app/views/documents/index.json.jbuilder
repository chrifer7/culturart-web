json.array!(@documents) do |document|
  json.extract! document, :id, :name, :description, :created_on, :tags
  json.url document_url(document, format: :json)
end
