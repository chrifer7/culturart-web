class CreateDocuments < ActiveRecord::Migration
  def change
    create_table :documents do |t|
      t.string :name
      t.text :description
      t.date :created_on
      t.string :tags

      t.timestamps
    end
  end
end
